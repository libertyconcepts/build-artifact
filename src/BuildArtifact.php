<?php
/**
 * Create a build artifact by removing unwanted files and directories from a project.
 */

namespace LibertyConcepts;

use \Composer\Script\Event;
use \Exception;
use \Symfony\Component\Yaml\Yaml;
use \Symfony\Component\Yaml\Exception\ParseException;

/**
 * Class BuildArtifact
 *
 * @package LibertyConcepts
 */
abstract class BuildArtifact
{
    /**
     * @var string
     */
    protected static $git_user_name;

    /**
     * @var string
     */
    protected static $git_user_email;

    /**
     * @var string
     */
    protected static $git_commit_id;

    /**
     * @var array
     */
    protected static $directories_to_remove = [];

    /**
     * @var array
     */
    protected static $files_to_remove = [];

    /**
     * The file
     *
     * @var string
     */
    protected static $build_artifact_config_file = 'build-artifact-config.yml';

    /**
     * @param Event $event
     */
    public static function init(Event $event)
    {
        $config = self::load_build_artifact_config($event);
        self::set_files_to_remove($config['files_to_remove']);
        self::set_directories_to_remove($config['directories_to_remove']);
    }

    /**
     * @param array $directories
     */
    protected static function set_directories_to_remove(array $directories)
    {
        self::$directories_to_remove = $directories;
    }

    /**
     * @param array $files
     */
    protected static function set_files_to_remove(array $files)
    {
        self::$files_to_remove = $files;
    }

    /**
     * @param Event $event
     *
     * @return mixed
     */
    public static function load_build_artifact_config(
        Event $event
    ) {
        try {
            $vendor_dir = $event->getComposer()->getConfig()->get('vendor-dir');
            $project_root = dirname($vendor_dir);
            $build_remove_file_path = $project_root
                . '/' . self::$build_artifact_config_file;

            if (!is_file($build_remove_file_path)) {
                $error = $build_remove_file_path.' file was not found in the project root, aborting build operation.';
                throw new Exception($error);
            }

            $build_ignore_config = Yaml::parseFile($build_remove_file_path);
            self::validate_build_remove_config($build_ignore_config);

            return $build_ignore_config;
        } catch (ParseException $exception) {
            printf('Unable to parse the YAML string: %s',
                $exception->getMessage());
        } catch (Exception $exception) {
            echo $exception->getMessage();
        }
    }

    /**
     * @param array $build_ignore_config
     *
     * @throws Exception Invalid build ignore configuration exception.
     */
    public static function validate_build_remove_config(array $build_ignore_config)
    {
        try {
            if (!isset($build_ignore_config['files_to_remove'])) {
                throw new Exception("Invalid build ignore config: missing key for files_to_remove");
            }

            if (!isset($build_ignore_config['directories_to_remove'])) {
                throw new Exception("Invalid build ignore config: missing key for directories_to_remove");
            }
        } catch (Exception $exception) {
            echo $exception->getMessage() . "\n";
            exit(1);
        }
    }

    /**
     * @param Event $event
     */
    public static function run(Event $event)
    {
        self::init($event);
        self::scrub_files(self::$files_to_remove);
        self::scrub_directories(self::$directories_to_remove);
    }

    /**
     * @param array $file_paths
     */
    protected static function scrub_files(array $file_paths)
    {
        if (empty($file_paths)) {
            echo "No files designated for scrubbing from the build artifact, moving on.";

            return;
        }

        echo "Removing individual files defined in " . self::$build_artifact_config_file . "\n";
        print_r($file_paths);
        foreach ($file_paths as $file_path) {
            $file_exists = file_exists($file_path);
            if ($file_exists) {
                $output = shell_exec("git rm $file_path");
                echo $output;
            } else {
                echo "  - File not found: $file_path. Moving on.\n";
            }
        }
    }

    /**
     * @param array $directory_paths
     */
    protected static function scrub_directories(array $directory_paths)
    {
        if (empty($directory_paths)) {
            echo "No directories designated for scrubbing from the build artifact, moving on.";

            return;
        }

        echo "Removing directories defined in " . self::$build_artifact_config_file . "\n";
        print_r($directory_paths);
        foreach ($directory_paths as $directory_path) {
            $directory_exists = is_dir($directory_path);
            if ($directory_exists) {
                $output = shell_exec("git rm -rf $directory_path");
                echo $output;
            } else {
                echo "  - Directory not found: $directory_path. Moving on.\n";
            }
        }
    }
}
