<?php

namespace LibertyConcepts;

use Composer\Script\Event;
use \Exception;

class CodeShip extends BuildArtifact
{
    /**
     * @inheritDoc
     */
    public static function init(Event $event)
    {
        try {
            if ('codeship' !== getenv('CI_NAME')) {
                throw new Exception('This can only be run from a CI build on Codeship.com, aborting operation.');
            }

            self::$git_user_name = getenv('CI_COMMITTER_NAME');
            self::$git_user_email = getenv('CI_COMMITTER_EMAIL');
            self::$git_commit_id = getenv('CI_COMMIT_ID');
            parent::init($event);
        } catch (Exception $exception) {
            echo $exception->getMessage() . "\n";
            exit(1);
        }
    }

    /**
     * @inheritDoc
     */
    public static function run(Event $event)
    {
        self::init($event);
        self::scrub_files(self::$files_to_remove);
        self::scrub_directories(self::$directories_to_remove);
        self::configure_git_user();
        self::git_commit();
    }

    /**
     * Configure the git user name and email address to use when committing the build artifact to a repo.
     */
    public static function configure_git_user()
    {
        shell_exec("git config --global user.email ".self::$git_user_email);
        shell_exec("git config --global user.name ".self::$git_user_name);
    }

    /**
     * Commit the clean build artifact to the current repo.
     */
    public static function git_commit(): void
    {
        $output = shell_exec("git add .");
        echo $output;
	    $output = shell_exec("git commit -m \"Sanitize build artifact.\"");
        echo $output;
	    $output = shell_exec("git reset --soft HEAD~2");
        echo $output;
        $output = shell_exec("git status");
        echo $output;
        $commit_message = "Automated build artifact created from commit "
            .self::$git_commit_id." by Codeship.";
        $output = shell_exec("git commit -m \"${commit_message}\"");
        echo $output;
    }
}
